﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using RabbitMQ.Client;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace TP2_signalR.Hubs
{
    public sealed class MessengerHub : Hub
    {
        struct MyMessage
        {
            public string user;
            public string message;
        }
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("RecieveMessage", user, message);

            // Sending the message via RabbitMQ
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "message",
                                       durable: false,
                                       exclusive: false,
                                       autoDelete: false,
                                       arguments: null);

                MyMessage myMessage = new MyMessage();
                myMessage.user = user;
                myMessage.message = message;

                // Parsing in json for sending
                string json = JsonConvert.SerializeObject(myMessage);


                var body = Encoding.UTF8.GetBytes(json);
                channel.BasicPublish(exchange: "",
                                     routingKey: "message",
                                     basicProperties: null,
                                     body: body);

            }
        }
    }
}
