﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Application
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("http://localhost:55470/stream")
                .AddMessagePackProtocol()
                .Build();

            await connection.StartAsync();

            var channel = await connection.StreamAsChannelAsync<int>("Counter", 10, 500);

            while (await channel.WaitToReadAsync())
            {
                while (channel.TryRead(out var count))
                {
                    Console.WriteLine(count);
                }
            }


            Console.ReadKey();
        }
    }
}
