﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TP2_signalR.Hubs;
using TP2_signalR.Models;

namespace TP2_signalR.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        private IHubContext<MessengerHub> m_context;
        public HomeController(IHubContext<MessengerHub> context)
        {
            m_context = context;
        }

        public async Task<IActionResult> Messenger()
        {
            await m_context.Clients.All.SendAsync("RecieveMessage", "system", "Nouveau user");
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
